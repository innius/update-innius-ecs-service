module bitbucket.org/innius/update-innius-ecs-service

go 1.12

require (
	github.com/aws/aws-sdk-go v1.24.4
	github.com/pkg/errors v0.8.1
	github.com/prometheus/common v0.7.0
)
