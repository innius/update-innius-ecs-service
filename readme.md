# update-innius-ecs-service 
updates an innius service task definition image with a specified docker image version 

## installation 
```
git clone https://bitbucket.org/innius/update-innius-ecs-service
go install 
```

## usage 
update-innius-ecs-service -cluster chair -service dashboard-service-v2 -image helloworld:latest

