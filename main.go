package main

import (
	"flag"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ecs"
	"github.com/aws/aws-sdk-go/service/ecs/ecsiface"
	"github.com/pkg/errors"
	"github.com/prometheus/common/log"
	"strings"
)

var client ecsiface.ECSAPI

func DockerImage(s *string) dockerImage {
	return dockerImage(aws.StringValue(s))
}

type dockerImage string

func (i dockerImage) name() string {
	img := string(i)
	idx1 := strings.Index(img, "/")
	idx2 := strings.Index(img, ":")
	return img[idx1+1 : idx2]
}

func main() {
	clusterName := flag.String("cluster", "chair", "the name of the innius stack")
	serviceName := flag.String("service", "", "the service you want to update")
	newImage := flag.String("image", "", "the container image to be applied")
	region := flag.String("region", endpoints.UsEast1RegionID, "the aws region of the cluster")
	flag.Parse()
	image := DockerImage(newImage)

	sess := session.Must(session.NewSession())

	client = ecs.New(sess, &aws.Config{Region: region})

	services, err := client.ListServices(&ecs.ListServicesInput{Cluster: clusterName, MaxResults: aws.Int64(100)})
	if err != nil {
		log.Fatal(err)
	}

	var serviceArn *string
	for _, arn := range services.ServiceArns {
		log.Info(aws.StringValue(arn))
		if strings.Contains(aws.StringValue(arn), aws.StringValue(serviceName)) {
			serviceArn = arn
			break
		}
	}
	if serviceArn == nil {
		log.Fatal(errors.Errorf("service %s does not exist", aws.StringValue(serviceName)))
	}
	res, err := client.DescribeServices(&ecs.DescribeServicesInput{
		Cluster:  clusterName,
		Services: []*string{serviceArn},
	})
	if err != nil {
		log.Fatal(err)
	}
	service := res.Services[0]

	output, err := client.DescribeTaskDefinition(&ecs.DescribeTaskDefinitionInput{TaskDefinition: service.TaskDefinition})
	if err != nil {
		log.Fatal(err)
	}
	taskDefinition := output.TaskDefinition
	for i := range taskDefinition.ContainerDefinitions {
		td := taskDefinition.ContainerDefinitions[i]
		img := DockerImage(td.Image)
		if img.name() == image.name() {
			log.Infof("updating container %s to %s", aws.StringValue(td.Image), image)
			td.Image = aws.String(string(image))
		}
	}

	log.Infof("register a new task revision for task %s", aws.StringValue(taskDefinition.TaskDefinitionArn))
	newRevision, err := client.RegisterTaskDefinition(&ecs.RegisterTaskDefinitionInput{
		Family:               taskDefinition.Family,
		TaskRoleArn:          taskDefinition.TaskRoleArn,
		ExecutionRoleArn:     taskDefinition.ExecutionRoleArn,
		ContainerDefinitions: taskDefinition.ContainerDefinitions,
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Infof("revision %d registered for task definition %s", aws.Int64Value(newRevision.TaskDefinition.Revision), aws.StringValue(newRevision.TaskDefinition.TaskDefinitionArn))
	updatedService, err := client.UpdateService(&ecs.UpdateServiceInput{
		Cluster:            clusterName,
		Service:            serviceArn,
		TaskDefinition:     newRevision.TaskDefinition.TaskDefinitionArn,
		ForceNewDeployment: aws.Bool(true),
	})
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(aws.StringValue(newRevision.TaskDefinition.TaskDefinitionArn))
	log.Info("waiting for deployment of new task revision")
	err = client.WaitUntilServicesStable(
		&ecs.DescribeServicesInput{
			Cluster:  clusterName,
			Services: []*string{updatedService.Service.ServiceName}})

	if err != nil {
		log.Fatal(err)
	}
	log.Info("service has been updated")
}
